# Obiz Shell Installation Script

Run the following command to install Obiz Shell. You'll need access to the private obiz-shell repository.

    curl -L https://bitbucket.org/obiz/shell-install/raw/master/install.sh | sh

If curl complains about the certificate not being valid (OSX 10.7 for example), you can follow [these instructions](http://www.simplicidade.org/notes/archives/2011/06/github_ssl_ca_errors.html) or just add a "k" option next to the "L" (insecure mode).
