if [ -d ~/.obiz ]
then
  echo "\033[0;33mYou already have Obiz Shell installed.\033[0m You'll need to remove ~/.obiz if you want to install"
  exit
fi

echo "\033[0;34mCloning Obiz Shell...\033[0m"
hash git >/dev/null && /usr/bin/env git clone git@bitbucket.org:obiz/shell.git ~/.obiz || {
  echo "git not installed"
  exit
}

echo "\033[0;34mLooking for an existing zsh config...\033[0m"
if [ -f ~/.zshrc ]
then
  echo "\033[0;33mFound ~/.zshrc.\033[0m \033[0;32 Backing up to ~/.zshrc.pre-obiz\033[0m";
  cp ~/.zshrc ~/.zshrc.pre-obiz;
else
	echo "zsh not installed. See https://github.com/robbyrussell/oh-my-zsh."
	exit
fi

echo "\033[0;34mSourcing Obiz Shell init script at the end of ~/.zshrc.\033[0m"
echo "source ~/.obiz/init.sh" >> ~/.zshrc

echo "\n\033[0;32mObiz Shell is now installed.\033[0m"

source ~/.obiz/init.sh
